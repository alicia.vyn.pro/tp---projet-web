<?php

// connexion à la BDD
define( 'DB_HOST', 'localhost' );
define( 'DB_USER', 'root' );
define( 'DB_PASS', '' );
define( 'DB_NAME', 'toys-r-us' );

// Ouverture de la connection à la base de données
$mysql = mysqli_connect( DB_HOST, DB_USER, DB_PASS, DB_NAME );

$stock_filters = [
    'store' => [
        'table' => 'stores',
        'default_message' => 'Choisissez un magasin'
    ],
];