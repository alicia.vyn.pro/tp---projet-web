<?php

// Variables pour l'affichage sur la vue
$html_stock = '';
$html_filters = '';

// Filtres en cours
$current_filters = [];

// array_keys() renvoie un tableau contenant seulement les clés d'un tableau donné
$filter_names = array_keys( $stock_filters );
foreach( $filter_names as $name ) {
    $current_filters[ $name ] = '';
}

// Requête de base pour remonter les voitures
$sql_stock = 'SELECT SUM(quantity) as total_stock
                FROM stock
                WHERE toy_id=?';

$stmt_params = [ 'i', &$toy_id ];

// Ajout des clauses WHERE pour les filtres
if( $_SERVER['REQUEST_METHOD'] === 'POST' ) {

    // Ininialisation des filtres
    // On boucle sur le tableau de configuration
    foreach( $stock_filters as $filter_name => $filter_properties ) {
        // Ce filtre n'est pas choisi par l'utilisateur ? => On passe au suivant
        if( empty( $_POST[ $filter_name ] ) ) {
            continue;
        }

// Récupération du choix pour ce filtre
$current_filters[ $filter_name ] = $_POST[ $filter_name ];

// Conversion du choix en type int
$int_current_filter = intval(  $current_filters[ $filter_name ], 10 );

// Ajout de la référence dans les paramètres du statement
$stmt_params[] = &$int_current_filter;

// Ajout du type du paramètre
$stmt_params[0] .= 'i';

    if( $filter_name === 'store' ){
        // Ajout de la clause WHERE pour ce filtre
        $sql_stock .= ' AND store_id = ?';
    }
    else{
        // Ajout de la clause WHERE pour ce filtre
        // On a décidé que les noms dans la base de données correspondaient au format:
        // `[nom_de_la_table_where]`.`[nom_du_filtre]_id`
        $sql_stock .= sprintf(' AND `%s`.`%s` = ?', $filter_properties[ 'where_table' ], $filter_name ); 
    }
}

}

$sql_stock .= ' GROUP BY toy_id';

// Construction des filtres à afficher
// On boucle sur le tableau de configuration
foreach( $stock_filters as $filter_name => $filter_properties ) {
    $html_filters .=html_filter_select(

    $filter_properties['table'],
    $filter_name,
    $filter_properties['default_message'],
    $current_filters[ $filter_name ]
    );
}

if( $stmt = mysqli_prepare( $mysql, $sql_stock ) ){

    array_unshift( $stmt_params, $stmt );

    call_user_func_array( 'mysqli_stmt_bind_param', $stmt_params );

    mysqli_stmt_execute( $stmt );

    $result = mysqli_stmt_get_result( $stmt );

    mysqli_stmt_close($stmt);

    $stock_row = mysqli_fetch_assoc( $result );

    $html_stock = '<span class="span-blue">Stock:</span> <span class="span-info">  ' . $stock_row['total_stock'] . '  </span>';

}
