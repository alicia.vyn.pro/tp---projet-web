<?php
// constantes de chemins
define( 'PATH_CONTROLLERS', 'app/controllers/' );
define( 'PATH_VIEWS', 'app/views/' );
define( 'PATH_MODELS', 'app/models/' );
define( 'PATH_PARTIALS', 'app/partials/' );

// demarrage session
session_start();

// connection de la BDD
$mysql = mysqli_connect( DB_HOST, DB_USER, DB_PASS, DB_NAME );

// si l'url n'est pas vide, aller sur l'url, sinon redirection vers les categories
$page = !empty( $_GET[ 'url' ] ) ? $_GET[ 'url' ] : 'accueil';

switch( $page ) {
    case 'accueil':
    require_once PATH_CONTROLLERS . 'home-controller.php';
        break;

    case 'liste' :
    require_once PATH_CONTROLLERS . 'list-controller.php';
        break;

    case 'details' :
    require_once PATH_CONTROLLERS . 'details-controller.php';
        break;

    default:
        break;
}