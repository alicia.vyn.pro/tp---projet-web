<?php

// AFFICHAGE DU TOP 3 DES VENTES
function html_home( mysqli_result $result ): string
{
    $html = '<ul class="item-ul">';

    while( $row = mysqli_fetch_assoc( $result ) ) {
        $html .= '<li class="item-li"> 
        <a href="http://tp-projet-web.test/?url=details&id=' . $row[ 'id' ] . '">
            <img src="media/' . $row[ 'image' ] . '" alt="">
            <h3 class="item-title">' . $row[ 'name' ] . '</h3>
            <p class="item-price">' . $row[ 'price' ] . ' €</p>
        </a> 
        </li>';
    }

    $html .= '</div>';

return $html;
}

function html_filter_select( string $table_name, string $attr_name, string $default_text, string $current_value ): string
{
    // On signale à la fonction que $mysql est une variable globale
    global $mysql;

    /*
    <select name="[$attr_name]">
        <option value="">[$default_text]</option>
        [option for $current_value: <option selected="selected" value="id">label</option>]
        [options from $table_name table]
    </select>
    */
    $html = sprintf( '<select name="%s"><option value="">%s</option>', $attr_name, $default_text );
    
    // Ajout des options
    $query = sprintf( 'SELECT * FROM `%s`', $table_name );

    if( $result = mysqli_query( $mysql, $query ) ) {
        while( $row = mysqli_fetch_assoc( $result ) ) {
            // Gestion de l'option à sélectionner
            $attr_selected = '';
            if( $row[ 'id' ] === $current_value ) {
                $attr_selected = ' selected="selected"';
            }

            $html .= sprintf( '<option%s value="%s">%s</option>', $attr_selected, $row[ 'id' ], $row[ 'name' ] );
        }
    }
    
    $html .= '</select>';

    return $html;
}


function html_brands( mysqli_result $result ): string
{
    $html = '<ul class="brands-ul">';

    while( $row = mysqli_fetch_assoc( $result ) ) {
        $html .= '<li class="brands-li">';
        $html .= '<a href="http://tp-projet-web.test/?url=liste&brands_id='. $row['id'];
        $html .= ' ">' . $row['name'] . '</a>';
        $html .= '</li>';
    }

    $html .= '</ul>';

return $html;

}


function html_brands_option( mysqli_result $result ): string
{
    $html = '';

    while( $row = mysqli_fetch_assoc( $result ) ) {
        $html .= '<option value="'. $row['id'];
        $html .= '">' . $row['name'] . '</option>'; 
    }
return $html; 
}

