<?php require_once PATH_MODELS . 'menu-model.php';?>

<div class="menu-wrapper">
    <ul class="menu-ul">
        <a href="http://tp-projet-web.test/?url=liste">
            <li class="menu-li">Tous les jouets</li>
        </a>
        <li class="menu-li-wrap">
            <span>Par marque <i class="fas fa-sort-down"></i></span>
            <?php echo $html_brands; ?>                
        </li>
    </ul>
</div>