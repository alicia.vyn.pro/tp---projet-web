<?php
require_once  'app/utils.php';
require_once PATH_MODELS . 'list-model.php';


$html_brands = '';
$html_list = '';
$html_brands_option = ''; 

$brands_id = !empty( $_GET[ 'brands_id' ] ) ? $_GET[ 'brands_id' ] : '';

// Menu marques
$brands = 'SELECT * FROM brands';

$result = mysqli_query( $mysql, $brands );
$html_brands = html_brands( $result );

$result = mysqli_query( $mysql, $brands );
$html_brands_option = html_brands_option( $result);


if( empty( $brands_id ) ) {
    $toys = 'SELECT * FROM toys';

    $stmt = mysqli_prepare( $mysql, $toys );

    mysqli_stmt_execute( $stmt );

    $result = mysqli_stmt_get_result( $stmt );

    $html_list = html_list( $result );
}
else {
    $brands = 'SELECT * FROM toys WHERE brand_id=?';

    $stmt = mysqli_prepare( $mysql, $brands );

    mysqli_stmt_bind_param($stmt, 'i', $brands_id);

    mysqli_stmt_execute( $stmt );

    $result = mysqli_stmt_get_result( $stmt );

    $html_list = html_list( $result );
}
