<?php
require_once  'app/utils.php';

function html_list( mysqli_result $result ): string
{
    $toys_html = '<div class="brands-wrapper">
    <select name="brands" id="brand-select">
        <option value="0">Quelle marque ?</option>
        <option value="1">Ravensburger</option>
        <option value="2">Asmodée</option>
        <option value="3">Mattel</option>
    </select>
    <input type="button" value="OK">
    </div>
    <ul class="item-ul">';

    while( $row = mysqli_fetch_assoc( $result ) ) {
        $toys_html .= '<li class="item-li"> 
        <a href="http://tp-projet-web.test/?url=details&id=' . $row[ 'id' ] . '">
            <img src="media/' . $row[ 'image' ] . '" alt="">
            <h3 class="item-title">' . $row[ 'name' ] . '</h3>
            <p class="item-price">' . $row[ 'price' ] . ' €</p>
        </a> 
        </li>';
    }

    $toys_html .= '</ul>';

return $toys_html;

}
