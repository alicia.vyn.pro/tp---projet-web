<?php
$toy_id = !empty( $_GET[ 'id' ] ) ? $_GET[ 'id' ] : '';

require_once  'app/utils.php';
require_once  'app/details-filter.php';


$detail = 'SELECT toys.*, 
            brands.name as brand
            FROM toys
            JOIN brands ON brand_id = brands.id 
            WHERE toys.id=? ';
    
if ( $stmt = mysqli_prepare( $mysql, $detail ) ) {

    mysqli_stmt_bind_param( $stmt, 'i', $toy_id );
    mysqli_stmt_execute( $stmt );

    $result = mysqli_stmt_get_result( $stmt );
        
    while( $row = mysqli_fetch_assoc( $result ) ) {        
        $detail_html = '<h2 class="page-title">' . $row['name'] . '</h2> 
        <div class="details-wrapper">
        <div class="details-left">
            <img src="media/' . $row[ 'image' ] . '" alt="' . $row[ 'image' ] . '">
            <p class="item-price-details">' . $row[ 'price' ] . ' €</p>
            <form method="post">
                ' . $html_filters . '

                <button type="submit">OK</button>
            </form>
            ' . $html_stock . '
        </div>
        <div class="details-right">
            <div>
            <span class="span-blue">Marque:</span> <span class="span-info"> '. $row[ 'brand' ] .' </span>
            </div> 
        ' . $row[ 'description' ] . '
        </div>
        </div>' ;
    }

}