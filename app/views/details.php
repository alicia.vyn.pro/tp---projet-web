<?php include_once PATH_PARTIALS . 'header-partial.php'; ?>

    <div id="wrapper">

        <?php include_once PATH_PARTIALS . 'logo-partial.php'; ?>

        <?php include_once PATH_PARTIALS . 'menu-partial.php'; ?>

        <?php echo $detail_html; ?>

    </div>

<?php include_once PATH_PARTIALS . 'footer-partial.php'; ?>