<?php include_once PATH_PARTIALS . 'header-partial.php'; ?>

    <div id="wrapper">

        <?php include_once PATH_PARTIALS . 'logo-partial.php'; ?>

        <?php include_once PATH_PARTIALS . 'menu-partial.php'; ?>

        <h1 class="page-title">Les jouets</h1>

        <?php echo $html_list; ?>

    </div>

<?php include_once PATH_PARTIALS . 'footer-partial.php'; ?>