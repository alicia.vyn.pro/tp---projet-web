<?php

// fichier de configurations
require_once 'app/config.php';

// fichiers utilitaires
require_once  'app/utils.php';

// initialisation de l'application
require_once 'app/app-start.php';

// fermeture de la connexion BDD
mysqli_close( $mysql );